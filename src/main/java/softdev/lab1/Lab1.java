/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package softdev.lab1;

import java.util.*;

/**
 *
 * @author EliteCorps
 */
public class Lab1 {

    static char turn;
    static char[][] pos;
    
    public Lab1() {
        pos = new char[3][3];
        turn = 'X';
        charPos();
    }
    
    public static void charPos() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                pos[i][j] = '-';
            }
        }
    }
    
    public static void gameBoard() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(pos[i][j] + " | ");
            }
            System.out.println("");
            System.out.println("-------------");
        }
    }
    
    public static void switchTurn() {
        if (turn == 'X') {
            turn = 'O';
        } else {
            turn = 'X';
        }
    }
    
    public boolean checkValidMove(int move) {
        int pRow = (move - 1) / 3;
        int pCol = (move - 1) % 3;
        if (move < 1 || move > 9) {
            System.out.println("Invalid move, please input number between 1-9");
            return false;
        }
        
        if (pos[pRow][pCol] != '-') {
            System.out.println("Invalid move, Position already taken");
            return false;
        }
        return true;
    }
    
    public static boolean checkWinner() {
        for (int i = 0; i < 3; i++) {
            if (pos[i][0] != '-' && pos[i][0] == pos[i][1] && pos[i][0] == pos[i][2]) {
                gameBoard();
                System.out.println("Game Over! " + pos[i][0] + " is the winner!!");
                return true;
            }
        }
        
        for (int i = 0; i < 3; i++) {
            if (pos[0][i] != '-' && pos[0][i] == pos[1][i] && pos[0][i] == pos[2][i]) {
                gameBoard();
                System.out.println("Game Over! " + pos[0][i] + " is the winner!!");
                return true;
            }
        }
        
        for (int i = 0; i < 3; i++) {
            if (pos[0][0] != '-' && pos[0][0] == pos[1][1] && pos[0][0] == pos[2][2]) {
                gameBoard();
                System.out.println("Game Over! " + pos[0][0] + " is the winner!!");
                return true;
            }
        }
        
        for (int i = 0; i < 3; i++) {
            if (pos[0][2] != '-' && pos[0][2] == pos[1][1] && pos[0][2] == pos[2][0]) {
                gameBoard();
                System.out.println("Game Over! " + pos[0][2] + " is the winner!!");
                return true;
            }
        }
        
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (pos[i][j] == '-') {
                    return false;
                }
            }
        }
        gameBoard();
        System.out.println("Game Over! it's a draw!!");
        return true;
    }
    
    public void playGame() {
        Scanner sc = new Scanner(System.in);
        int move;
        boolean reset = true;
        String resetInput;
        
        while (reset) {
            charPos();
            turn = 'X';
            
            while (!checkWinner()) {                
                gameBoard();
                System.out.println("Current turn: " + turn);
                System.out.print("Please Enter your move (1 - 9): ");
                move = sc.nextInt();
                if (checkValidMove(move)) {
                    int pRow = (move - 1) / 3;
                    int pCol = (move - 1) % 3;
                    pos[pRow][pCol] = turn;
                    switchTurn();
                }
            }
            
            System.out.print("Do you want to play again? (y/n): ");
            resetInput = sc.next();
            reset = resetInput.equalsIgnoreCase("y");
        }
        
    }
    
    public static void main(String[] args) {
        Lab1 game = new Lab1();
        game.playGame();
    }
}
